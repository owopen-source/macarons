import secrets as sc
import time

from machine import Pin, PWM
from utime import sleep
from utils import get_config, ota_update, send_readings, script_done
import ujson

with open('config.json', 'r') as f:
    config = ujson.load(f)
with open('readings.json', 'r') as f:
    readings = ujson.load(f)


class LockSetter:
    def __init__(self, gpio_lock, loc2duty):
        self.p_lock = Pin(gpio_lock, Pin.OUT)
        self.loc2duty = loc2duty

    def set_lock(self, lock):
        if lock:
            pwm = PWM(self.p_lock)
            pwm.freq(50)
            pwm.duty(self.loc2duty[1])
            sleep(1.0)
            pwm.deinit()
        else:
            pwm = PWM(self.p_lock)
            pwm.freq(50)
            pwm.duty(self.loc2duty[0])
            sleep(1.0)
            pwm.deinit()


class MovementSetter:
    def __init__(self, gpio_left_1, gpio_left_2, gpio_left_en, speed_up, speed_down, stop_time_up, stop_time_down):
        self.p_1 = Pin(gpio_left_1, Pin.OUT)
        self.p_2 = Pin(gpio_left_2, Pin.OUT)
        self.p_en = Pin(gpio_left_en, Pin.OUT)
        self.speed_up = speed_up
        self.speed_down = speed_down
        self.stop_time_up = stop_time_up
        self.stop_time_down = stop_time_down

        self.pwm = PWM(self.p_en, freq=50)
        self.up = True
        self.speed = 0.5

    def start_move(self, up):
        self.up = up
        self._set_direction(up=up)
        if up:
            self._set_speed(self.speed_up)
        else:
            self._set_speed(self.speed_down)

    def stop_move(self):
        stop_time = self.stop_time_up if self.up else self.stop_time_down
        self._set_direction(up=not self.up)
        sleep(stop_time)
        self._set_speed(0)
        self._set_direction(up=not self.up)

    def _set_speed(self, speed):
        # speed is between 0 and 1
        self.pwm.duty(int(1023*speed))

    def _set_direction(self, up):
        self.up = up
        if up:
            self.p_1.on()
            self.p_2.off()
        else:
            self.p_1.off()
            self.p_2.on()


class InfraredSensor:
    def __init__(self, gpio_infrared, rate):
        self.p_infrared = Pin(gpio_infrared, Pin.IN)
        self.rate = rate
        self.period = 1 / rate

    def read_sensor(self):
        return self.p_infrared.value()

    def block_until_reflection(self, reverse=False):
        init_value = 0 if reverse else 1
        reading = init_value
        while reading == init_value:
            reading = self.read_sensor()
            sleep(self.period)


# initialisation
if config['awake']:
    movement_setter = MovementSetter(gpio_left_1=18, gpio_left_2=10, gpio_left_en=19,
                                     speed_up=1.0, speed_down=1.0, stop_time_up=0.05, stop_time_down=0.05)
    lock_setter = LockSetter(gpio_lock=9, loc2duty=[55, 90])
    infrared_sensor = InfraredSensor(gpio_infrared=0, rate=40)

# awake loop
move_command = False
while config['awake']:
    # go to target
    if config['go_to_target']:
        v_command = config['v_target'] - readings['v_position'][0]
        direction = 1 if v_command >= 0 else -1
        move_command = v_command != 0
        up_command = direction > 0

        # begin movement
        if move_command:
            # delta up
            movement_setter.start_move(up=True)
            time.sleep(config['t_delta'])
            movement_setter.stop_move()
            sleep(0.5)
            lock_setter.set_lock(lock=False)

            movement_setter.start_move(up=up_command)
            sleep(config['grace_time'])
            infrared_sensor.block_until_reflection()
            if up_command:
                infrared_sensor.block_until_reflection(reverse=True)
            movement_setter.stop_move()     # todo: currently only one floor, change if more than one floors
            sleep(0.5)

            # delta up
            movement_setter.start_move(up=True)
            time.sleep(config['t_delta'])
            movement_setter.stop_move()
            sleep(0.5)

            lock_setter.set_lock(lock=True)
            sleep(0.5)
            movement_setter.start_move(up=False)
            time.sleep(1.2*config['t_delta'])
            movement_setter.stop_move()

            readings['v_position'][0] += direction      # todo: currently only one floor, change if more than one floors
            with open('readings.json', 'w') as f:
                ujson.dump(readings, f)
            send_readings(readings)

    # update config parameters
    if config['script_pk'] != -1:
        script_done(script_pk=config['script_pk'], device_pk=sc.pk)
    sleep(config['update_period'])
    config = get_config()
