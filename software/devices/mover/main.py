import secrets as sc
from machine import Pin, PWM
from utime import sleep
from utils import get_config, ota_update, send_readings, script_done
import ujson

with open('config.json', 'r') as f:
    config = ujson.load(f)
with open('readings.json', 'r') as f:
    r = ujson.load(f)
readings = {'h_position': r['h_position']}


class MovementSetter:
    def __init__(self, gpio_left_1, gpio_left_2, gpio_left_en, gpio_right_1, gpio_right_2, gpio_right_en):
        self.p_left_1 = Pin(gpio_left_1, Pin.OUT)
        self.p_left_2 = Pin(gpio_left_2, Pin.OUT)
        self.p_left_en = Pin(gpio_left_en, Pin.OUT)
        self.p_right_1 = Pin(gpio_right_1, Pin.OUT)
        self.p_right_2 = Pin(gpio_right_2, Pin.OUT)
        self.p_right_en = Pin(gpio_right_en, Pin.OUT)

        self.move = False
        self.forward = True

    def set_move(self, move=False, forward=True):
        self.move = move
        self.forward = forward

        if self.move:
            self._set_direction()
            self.p_left_en.on()
            self.p_right_en.on()
        else:
            self.p_left_en.off()
            self.p_right_en.off()

    def _set_direction(self):
        if self.forward:
            self.p_left_1.off()
            self.p_left_2.on()
            self.p_right_1.off()
            self.p_right_2.on()
        else:
            self.p_left_1.on()
            self.p_left_2.off()
            self.p_right_1.on()
            self.p_right_2.off()


class InteractionSetter:
    def __init__(self, gpio_interact, loc2duty):
        self.p_interact = Pin(gpio_interact, Pin.OUT)
        self.loc2duty = loc2duty
        self.current_interact = False

    def set_interact(self, interact):
        if not interact == self.current_interact:
            if interact:
                pwm_interact = PWM(self.p_interact)
                pwm_interact.freq(50)
                pwm_interact.duty(self.loc2duty[1])
                sleep(1.0)
                pwm_interact.deinit()
            else:
                pwm_interact = PWM(self.p_interact)
                pwm_interact.freq(50)
                pwm_interact.duty(self.loc2duty[0])
                sleep(1.0)
                pwm_interact.deinit()
            self.current_interact = interact


class PseudoDeque:
    def __init__(self, memory=[], max_len=10):
        assert len(memory) <= max_len, 'provided list is longer than max_len'
        self.memory = memory
        self.max_len = max_len

    def __repr__(self):
        return str(self.memory)

    def __str__(self):
        return str(self.memory)

    def append(self, x):
        if len(self.memory) == self.max_len:
            self.memory.pop(0)
        self.memory.append(x)


class InfraredReading:
    def __init__(self, gpio_infrared, buffer_length=10):
        self.sensor_buffer = PseudoDeque([1 for _ in range(buffer_length)], buffer_length)
        self.buffer_length = buffer_length
        self.p_infrared = Pin(gpio_infrared, Pin.IN)

        self.tick_counter = 0
        self.current_state = 1

    def read_sensor(self):
        self.sensor_buffer.append(self.p_infrared.value())
        new_state = self.signal_from_buffer()
        if self.current_state - new_state == -1:
            self.tick_counter += 1
        self.current_state = new_state

    def signal_from_buffer(self):
        return 0 if self.sensor_buffer.memory.count(0) > self.sensor_buffer.memory.count(1) else 1


# initialisation
if config['awake']:
    movement_setter = MovementSetter(gpio_left_1=18, gpio_left_2=10, gpio_left_en=19,
                                     gpio_right_1=9, gpio_right_2=8, gpio_right_en=6)
    interaction_setter = InteractionSetter(gpio_interact=0, loc2duty=[95, 45])
    infrared_reading = InfraredReading(gpio_infrared=7, buffer_length=10)

# awake loop
while config['awake']:
    # translation motor
    if config['go_to_target']:
        h_command = config['h_target'] - readings['h_position'][0]
        direction = 1 if h_command >= 0 else -1
        move_command = h_command != 0
        forward_command = direction > 0

        # begin movement
        if move_command:
            interaction_setter.set_interact(interact=config['deliver'])
            movement_setter.set_move(move=move_command, forward=forward_command)
            sleep(config['grace_time'])

            # loop movement until passes termination check
            while move_command:
                infrared_reading.read_sensor()

                # if infrared reader ticked then update h_position
                if infrared_reading.tick_counter == 1:
                    readings['h_position'][0] += direction
                    with open('readings.json', 'w') as f:
                        r['h_position'] = readings['h_position']
                        ujson.dump(r, f)
                    send_readings(readings)
                    infrared_reading.tick_counter = 0
                move_command = config['h_target'] - readings['h_position'][0] != 0
                sleep(0.05)
            # termination movement
            if config['deliver']:
                sleep(config['overshoot_time'])
                movement_setter.set_move(move=False, forward=forward_command)
                sleep(1)
                movement_setter.set_move(move=True, forward=not forward_command)
                sleep(config['overshoot_time'])
                movement_setter.set_move(move=False, forward=forward_command)
                sleep(1)
                interaction_setter.set_interact(interact=False)
            else:
                movement_setter.set_move(move=False, forward=forward_command)

    else:
        # interaction servo
        interaction_setter.set_interact(config['interact'])
        # translation motor
        movement_setter.set_move(move=config['move'], forward=config['forward'])

    # update config parameters
    if config['script_pk'] != -1:
        script_done(script_pk=config['script_pk'], device_pk=sc.pk)
    sleep(config['update_period'])
    config = get_config()
