import secrets as sc
from machine import Pin, PWM
from utime import sleep
from utils import get_config, ota_update, script_done
import ujson


with open('config.json', 'r') as f:
    config = ujson.load(f)
with open('readings.json', 'r') as f:
    readings = ujson.load(f)


while config['awake']:
    # insert code here
    # ================

    # ================

    if config['script_pk'] != -1:
        script_done(script_pk=config['script_pk'], device_pk=sc.pk)
    sleep(config['update_period'])
    config = get_config()

