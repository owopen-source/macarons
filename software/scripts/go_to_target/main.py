import json
from django.core.management.base import BaseCommand
from macarons_app.models import Device, Script


class Command(BaseCommand):
    help = 'Takes mover to target position'

    def add_arguments(self, parser):
        parser.add_argument('script_pk', type=int, help='Script primary key')

    def handle(self, *args, **kwargs):
        mover_pk = 2
        elevator_pk = 1

        script_pk = kwargs['script_pk']
        script = Script.objects.get(pk=script_pk)
        config = json.loads(script.config)
        readings = json.loads(script.readings)

        # check if all devices are done
        if all(done for done in readings['devices_done'][0].values()):

            # update script readings
            readings['step'][0] += 1

            elevator = Device.objects.get(pk=elevator_pk)
            elevator_readings = json.loads(elevator.readings)
            readings["elevator_v_pos"][0] = elevator_readings['v_position'][0]

            mover = Device.objects.get(pk=mover_pk)
            mover_readings = json.loads(mover.readings)
            readings["mover_h_pos"][0] = mover_readings['h_position'][0]
            readings["mover_v_pos"][0] = mover_readings['v_position'][0]

            # termination check
            terminate = readings['step'][0] >= 5
            readings['script_ready'][0] = terminate

            if terminate:
                readings['step'][0] = -1
            else:
                if readings['step'][0] == 0:

                    if readings['mover_v_pos'][0] != config['target_v_pos']:
                        config_list = [(elevator_pk, {"script_pk": script_pk,
                                                      "v_target": readings['mover_v_pos'][0],
                                                      "go_to_target": True}),
                                       ]
                    else:
                        readings['step'][0] = 2

                if readings['step'][0] == 1:
                    config_list = [(mover_pk, {"script_pk": script_pk,
                                               "go_to_target": True,
                                               "deliver": False, "h_target": 0}),
                                   ]

                if readings['step'][0] == 2:
                    if readings['mover_h_pos'] != config['target_h_pos']:
                        config_list = [(elevator_pk, {"script_pk": script_pk,
                                                      "v_target": config['target_v_pos'],
                                                      "go_to_target": True}),
                                       ]
                        readings["mover_v_pos"][0] = config['target_v_pos']
                        mover_readings['v_position'][0] = config['target_v_pos']
                        mover.readings = json.dumps(mover_readings)
                        mover.save()
                    else:
                        readings['step'][0] = 4

                if readings['step'][0] == 3:
                    if readings['mover_h_pos'] != config['target_h_pos']:
                        config_list = [(mover_pk, {"script_pk": script_pk,
                                                   "go_to_target": True,
                                                   "deliver": False, "h_target": config['target_h_pos']}),
                                       ]
                    else:
                        readings['step'][0] += 1

                # pre-terminate, revert to default config
                if readings['step'][0] == 4:
                    config_list = [(mover_pk, {"script_pk": script_pk,
                                               "go_to_target": False,
                                               "interact": False
                                               }),
                                   (elevator_pk, {"script_pk": script_pk,
                                                  "go_to_target": False}),
                                   ]

                # send to database
                for device_pk, new_config in config_list:
                    device = Device.objects.get(pk=device_pk)
                    config = json.loads(device.config)
                    readings['devices_done'][0][f'pk{device_pk}'] = False

                    for key in new_config:  # todo: maybe we can do type checker
                        config[key] = new_config[key]
                    device.staged_config = True
                    device.config = json.dumps(config)
                    device.save()

            script.readings = json.dumps(readings)
            script.save()
