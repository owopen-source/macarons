import json
from django.core.management.base import BaseCommand
from test_app.models import Device, Script


class Command(BaseCommand):
    help = 'Help message'

    def add_arguments(self, parser):
        parser.add_argument('script_pk', type=int, help='Script primary key')

    def handle(self, *args, **kwargs):
        script_pk = kwargs['script_pk']
        script = Script.objects.get(pk=script_pk)
        config = json.loads(script.config)
        readings = json.loads(script.readings)

        # check if all devices are done
        if all(done for done in readings['devices_done'][0].values()):

            # process readings here
            # =====================

            # =====================

            # based on readings, check for termination here
            # =============================================
            terminate = True
            readings['script_ready'][0] = terminate

            # =============================================

            if terminate:
                pass
            else:
                # generate config_list
                # ====================

                device_pk = 1
                config_list = [(device_pk, {"script_pk": script_pk,
                                            # add configs here
                                            }),
                               # can add multiple devices
                               ]
                # ====================

                # send to database
                for device_pk, new_config in config_list:
                    device = Device.objects.get(pk=device_pk)
                    config = json.loads(device.config)
                    readings['devices_done'][0][f'pk{device_pk}'] = False
                    for key in new_config:
                        config[key] = new_config[key]
                    device.staged_config = True
                    device.config = json.dumps(config)
                    device.save()
            script.readings = json.dumps(readings)
            script.save()