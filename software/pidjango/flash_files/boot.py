import esp
esp.osdebug(None)
import gc
gc.collect()
import machine
from utils import connect2wifi, ota_update  # safer to place before import ujson
import ujson

wifi_connected = connect2wifi()
if wifi_connected:
    config = ota_update()

# run main.py
import main

# loop forever
if wifi_connected:
    config = ota_update()
else:
    with open('config.json', 'r') as f:
        config = ujson.load(f)

print('sleeping for {} s'.format(config['sleep_time']))
machine.deepsleep(int(config['sleep_time']*1000))   # machine.deepsleep is in microseconds
