import os
import network
import utime
import secrets as sc


def connect2wifi():
    station = network.WLAN(network.STA_IF)
    station.active(False)
    station.active(True)
    station.connect(sc.ssid, sc.password)

    print('connecting to wifi')
    t_start = utime.time()
    while not station.isconnected():
        if utime.time() - t_start > 10:
            print('timed out, failed to connect to wifi.')
            return False
    print('successfuly connected to wifi.')
    return True


try:
    import ujson
except ImportError:
    wifi_connected = connect2wifi()
    if wifi_connected:
        import upip
        upip.install('ujson')
        import ujson
try:
    import urequests
except ImportError:
    wifi_connected = connect2wifi()
    if wifi_connected:
        import upip
        upip.install('urequests')
        import urequests


def ota_update():
    url = sc.host_url + '/api_update'
    print('checking for updates')

    try:
        r = urequests.post(url, data=ujson.dumps({'pk': sc.pk, 'done': False}))

    except OSError:
        print('No response from server, is the server down?')

    else:
        data = ujson.loads(r.text)
        if data:
            for key, value in data.items():
                file = '{}.py'.format(key)
                print('{} update found'.format(file))

                json_obj = ujson.loads(value)
                if file in os.listdir():
                    os.remove(file)

                if key == 'main':
                    with open(file, 'w+') as f:
                        for line in json_obj:
                            f.write(line)

                elif key == 'config':
                    with open('config.json', 'w') as f:
                        ujson.dump(json_obj, f)

                elif key == 'readings':
                    with open('readings.json', 'w') as f:
                        ujson.dump(json_obj, f)

            # update response
            print('updated')
            _ = urequests.post(url, data=ujson.dumps({'pk': sc.pk, 'done': True}))
        else:
            print('no update found')

    with open('config.json', 'r') as f:
        config = ujson.load(f)
    return config


def get_config():
    url = sc.host_url + '/api_config'
    print('getting configs')

    try:
        r = urequests.get(url, data=ujson.dumps({'pk': sc.pk}))

    except OSError:
        print('No response from server, is the server down?')
        with open('config.json', 'r') as f:
            config = ujson.load(f)
        return config

    else:
        return ujson.loads(r.text)


def send_readings(readings):
    url = sc.host_url + '/api_readings'
    print('sending readings')

    try:
        _ = urequests.post(url, data=ujson.dumps({'pk': sc.pk,
                                                 'readings': readings}))

    except OSError:
        print('No response from server, is the server down?')


def script_done(script_pk, device_pk):
    url = sc.host_url + '/api_script'
    print('calling script api')

    try:
        _ = urequests.post(url, data=ujson.dumps({'script_pk': script_pk,
                                                  'device_pk': device_pk}))
    except OSError:
        print('No response from server, is the server down?')
