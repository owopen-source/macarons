"""pidjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from macarons_app import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.IndexView.as_view(), name='index_view'),
    path('device_list', views.DeviceListView.as_view(), name='device_list_view'),
    path('add_device', views.AddDeviceView.as_view(), name='add_device_view'),
    path('device_update/<pk>', views.DeviceUpdateView.as_view(), name='update_device_view'),
    path('script_list', views.ScriptListView.as_view(), name='script_list_view'),
    path('add_script', views.AddScriptView.as_view(), name='add_script_view'),
    path('script_update/<pk>', views.ScriptUpdateView.as_view(), name='update_script_view'),
    path('api_update', views.DeviceUpdateAPI.as_view(), name='api_update_view'),
    path('api_config', views.DeviceConfigAPI.as_view(), name='api_config_view'),
    path('api_readings', views.DeviceReadingsAPI.as_view(), name='api_readings_view'),
    path('api_script', views.ScriptAPI.as_view(), name='api_script_view'),
]
