from django.db import models


# Create your models here.
class Device(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True)
    device_type = models.TextField(blank=False)

    staged_main = models.BooleanField(default=False)
    staged_config = models.BooleanField(default=False)
    staged_readings = models.BooleanField(default=False)

    main = models.TextField(blank=False)
    config = models.TextField(blank=True, null=True)
    readings = models.TextField(blank=True, null=True)

    config_type = models.TextField(blank=True, null=True)


class Script(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField(blank=True)

    main = models.TextField(blank=False)
    config = models.TextField(blank=True, null=True)
    readings = models.TextField(blank=True, null=True)

    config_type = models.TextField(blank=True, null=True)


# todo: probably do tracking here things instead
# class ReadingsLog(models.Model):
#     device = models.ForeignKey(Device, on_delete=models.CASCADE)
#     name = models.CharField(max_length=30)
#
#     data = models.TextField(blank=True, null=True)
#     date_time = models.TextField(blank=True, null=True)
#
#     def __str__(self):
#         return self.name
