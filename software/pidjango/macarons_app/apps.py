from django.apps import AppConfig


class MacaronsAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'macarons_app'
