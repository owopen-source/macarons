from django.contrib import admin
from macarons_app.models import Device, Script


class DeviceAdmin(admin.ModelAdmin):
    fields = ['name',
              'description',
              'device_type',
              'staged_main',
              'staged_config',
              'staged_readings',
              'main',
              'config',
              'readings',
              'config_type']


class ScriptAdmin(admin.ModelAdmin):
    fields = ['name',
              'description',
              'main',
              'config',
              'readings',
              'config_type']


admin.site.register(Device, DeviceAdmin)
admin.site.register(Script, ScriptAdmin)
