import os
import time
import json
from django.core.management import execute_from_command_line
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.generic import TemplateView, ListView
from macarons_app.models import Device, Script
from django.views import View

import subprocess
from pidjango import django_secrets as ds


def run_subprocess(command):
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    process.wait()
    return process.communicate()


def flash_device(device, context):
    # micropython version for device type
    # todo need to make sure version is installed on the rpi
    # todo, maybe pull latest version etc.

    # Create flash commands
    file_path = 'flash_files/'
    check_connect = ["esptool.py", "--port", "/dev/ttyUSB0", "flash_id"]
    flash_template = ["ampy", "--port", "/dev/ttyUSB0", "--baud", "115200", 'put']

    # device-specific flash commands
    if device.device_type == 'ESP32-WROOM-32':
        upython_bin = 'esp32-ota-20220117-v1.18.bin'
        erase_flash = ['esptool.py', '--chip', 'esp32', '--port', '/dev/ttyUSB0', 'erase_flash']
        flash_upython = ['esptool.py', '--chip', 'esp32', '--port', '/dev/ttyUSB0', '--baud', '460800', 'write_flash',
                         '-z', '0x1000', file_path+upython_bin]
    elif device.device_type == 'ESP32-C3-12F':
        upython_bin = 'esp32c3-20220117-v1.18.bin'
        erase_flash = ['esptool.py', '--chip', 'esp32c3', '--port', '/dev/ttyUSB0', 'erase_flash']
        flash_upython = ['esptool.py', '--chip', 'esp32c3', '--port', '/dev/ttyUSB0', '--baud', '460800', 'write_flash',
                         '-z', '0x0', file_path+upython_bin]
    else:
        context['flash_error'] = 'Device type not found'
        return context

    # file flash commands
    filename_list = [file_path + 'utils.py', 'secrets.py', 'config.json', 'readings.json',
                     'main.py', file_path + 'boot.py']
    lines_dict = {'secrets.py': [f"host_ip = '{ds.host_ip}'\n",
                                 f"host_port = '{ds.host_port}'\n",
                                 "host_url = 'http://' + host_ip + ':' + host_port\n",
                                 f"ssid = '{ds.ssid}'\n",
                                 f"password = '{ds.password}'\n",
                                 f"pk = {device.pk}\n"],
                  'main.py': json.loads(device.main),
                  'config.json': device.config,
                  'readings.json': device.readings}

    # check connection
    print('Checking connection...')
    result = run_subprocess(check_connect)
    if result[-1]:
        context['flash_error'] = 'Could not connect to device.'
        return context

    # erase flash
    print('Erasing chip...')
    result = run_subprocess(erase_flash)
    if result[-1]:
        context['flash_error'] = 'Could not erase the chip'
        return context

    # flash micropython
    print('Flashing micropython...')
    result = run_subprocess(flash_upython)
    if result[-1]:
        context['flash_error'] = 'Could not flash micropython'
        return context
    time.sleep(5)

    # flash files
    for filename in filename_list:
        print(f'Flashing {filename}...')
        if filename in ['secrets.py', 'main.py', 'config.json', 'readings.json']:
            with open(filename, 'w+') as f:
                for line in lines_dict[filename]:
                    f.write(line)
        flash_command = flash_template.copy()
        flash_command.append(filename)
        result = run_subprocess(flash_command)
        if filename in ['secrets.py', 'main.py', 'config.json', 'readings.json']:
            os.remove(filename)
        if result[-1]:
            context['flash_error'] = f'Failed to flash {filename}'
            return context

    print('Flash successful.')
    return context


class AddDeviceView(View):
    def get(self, request, *args, **kwargs):
        return render(request, "macarons_app/add_device.html")

    def post(self, request, *args, **kwargs):
        device = Device(name=request.POST.get('name'),
                        description=request.POST.get('description'),
                        device_type=request.POST.get('device_type'))

        context = {'name': request.POST.get('name'),
                   'description': request.POST.get('description'),
                   'device_type': request.POST.get('device_type')}
        form_valid = True

        # main file
        try:
            main_file = request.FILES['main_file']
            if main_file.name.split('.')[-1] != 'py':
                form_valid = False
                context['main_error'] = 'Main file must be a .py file.'
            else:
                device.main = json.dumps([line.decode('utf-8').replace('\r', '') for line in main_file])
                device.staged_main = False
        except KeyError:
            form_valid = False
            context['main_error'] = 'Device must contain a main.py file.'

        # config file
        try:
            config_file = request.FILES['config_file']
            if config_file.name.split('.')[-1] != 'json':
                form_valid = False
                context['config_error'] = 'Config file must be a .json file.'
            else:
                config = json.loads(config_file.read().decode('utf-8'))
                config_type = {}
                for key in config:
                    config_type[key] = type(config[key]).__name__
                device.config_type = json.dumps(config_type)
                device.config = json.dumps(config)
                device.staged_config = False
        except KeyError:
            form_valid = False
            context['config_error'] = 'Device must contain a config.json file.'

        # readings file
        try:
            readings_file = request.FILES['readings_file']
            if readings_file.name.split('.')[-1] != 'json':
                form_valid = False
                context['readings_error'] = 'Readings file must be a .json file.'
            else:
                readings = json.loads(readings_file.read().decode('utf-8'))

                # todo: generate readingslog stuff, or do firebase thing
                device.readings = json.dumps(readings)
        except KeyError:
            form_valid = False
            context['readings_error'] = 'Device must contain a readings.json file.'

        if form_valid:
            device.save()   # need to save to generate device.pk (used in flash_device)
            context = flash_device(device, context)
            if 'flash_error' in context:
                device.delete()
                return render(request, "macarons_app/add_device.html", context)
            else:
                # todo: loading screen
                return redirect('update_device_view', pk=device.pk)
        else:
            return render(request, "macarons_app/add_device.html", context)


class DeviceListView(ListView):
    model = Device
    template_name = "macarons_app/device_list.html"


class DeviceUpdateView(View):
    def get(self, request, *args, **kwargs):
        device = Device.objects.get(pk=kwargs['pk'])

        context = {'pk': kwargs['pk'],
                   'name': device.name,
                   'description': device.description,
                   'main': json.loads(device.main)}

        config = json.loads(device.config)
        config_type = json.loads(device.config_type)
        kvt = []
        for key in config:
            kvt.append([key, config[key], config_type[key]])
        context['kvt'] = kvt
        context['script_pk'] = config['script_pk']

        if device.readings:
            context['readings'] = json.loads(device.readings)

        context['staged_main'] = device.staged_main
        context['staged_config'] = device.staged_config

        return render(request, "macarons_app/update_device.html", context)

    def post(self, request, *args, **kwargs):
        device = Device.objects.get(pk=kwargs['pk'])
        context = {'pk': kwargs['pk']}

        # general
        device.name = request.POST.get('name')
        context['name'] = device.name
        device.description = request.POST.get('description')
        context['description'] = device.description

        # config
        no_config_upload = True
        try:
            config_file = request.FILES['config_file']
            if config_file.name.split('.')[-1] != 'json':
                context['config_upload_error'] = 'Config file must be a .json file.'
            else:
                no_config_upload = False
                config = json.loads(config_file.read().decode('utf-8'))
                config_type = {}
                for key in config:
                    config_type[key] = type(config[key]).__name__
                device.config_type = json.dumps(config_type)
                device.config = json.dumps(config)
                device.staged_config = True

                kvt = []
                for key in config:
                    kvt.append([key, config[key], config_type[key]])
                context['kvt'] = kvt
        except KeyError:
            pass

        if no_config_upload:
            config = json.loads(device.config)
            config_type = json.loads(device.config_type)

            kvt = []
            incorrect_keys = []

            for i, key in enumerate(config):
                if key == 'script_pk':
                    pass    # does not allow for script_pk to be modified
                else:
                    # boolean (value is constrained by drop down box to be either 't' or 'f', always correct type)
                    if config_type[key] == 'bool':
                        value = request.POST.get(key) == 't'
                        kvt.append([key, value, config_type[key]])
                        if config[key] != value:
                            config[key] = value
                            device.staged_config = True

                    elif config_type[key] == 'int':
                        try:
                            value = int(request.POST.get(key))
                            kvt.append([key, value, config_type[key]])
                            if config[key] != value:
                                config[key] = value
                                device.staged_config = True
                        except ValueError:
                            incorrect_keys.append(key)
                            kvt.append([key, config[key], config_type[key]])

                    elif config_type[key] == 'float':
                        try:
                            value = float(request.POST.get(key))
                            kvt.append([key, value, config_type[key]])
                            if config[key] != value:
                                config[key] = value
                                device.staged_config = True
                        except ValueError:
                            incorrect_keys.append(key)
                            kvt.append([key, config[key], config_type[key]])

                    elif config_type[key] == 'str':
                        kvt.append([key, request.POST.get(key), config_type[key]])
                        if config[key] != request.POST.get(key):
                            config[key] = request.POST.get(key)
                            device.staged_config = True

            device.config = json.dumps(config)
            context['kvt'] = kvt
            context['script_pk'] = config['script_pk']
            if incorrect_keys:
                context['config_error'] = 'Wrong variable type for: {}'.format(incorrect_keys)
        context['staged_config'] = device.staged_config

        # readings
        no_readings_upload = True
        try:
            readings_file = request.FILES['readings_file']
            if readings_file.name.split('.')[-1] != 'json':
                context['readings_upload_error'] = 'Readings file must be a .json file.'
            else:
                no_readings_upload = False
                readings = json.loads(readings_file.read().decode('utf-8'))

                # todo: generate readingslog stuff, or do firebase thing
                device.readings = json.dumps(readings)
                device.staged_readings = True
                context['readings'] = readings
                context['staged_readings'] = device.staged_readings
        except KeyError:
            pass

        if no_readings_upload:
            readings = json.loads(device.readings)
            context['readings'] = readings

        # main
        try:
            main_file = request.FILES['main_file']
            if main_file.name.split('.')[-1] != 'py':
                context['main_error'] = 'Main file must be a .py file.'
            else:
                device.main = json.dumps([line.decode('utf-8').replace('\r', '') for line in main_file])
                device.staged_main = True
                context['main'] = device.main
        except KeyError:
            pass

        context['main'] = device.main
        context['staged_main'] = device.staged_main

        device.save()
        return render(request, "macarons_app/update_device.html", context)


class DeviceUpdateAPI(View):
    def post(self, request):
        data = json.loads(request.body)
        device = Device.objects.get(pk=data['pk'])
        content = {}

        # main
        if not data['done']:
            if device.staged_main:
                content['main'] = device.main
            if device.staged_config:
                content['config'] = device.config
            if device.staged_readings:
                content['readings'] = device.readings
        else:
            device.staged_main = False
            device.staged_config = False
            device.staged_readings = False
            device.save()
        return HttpResponse(json.dumps(content))


class DeviceConfigAPI(View):
    def get(self, request):
        data = json.loads(request.body)
        device = Device.objects.get(pk=data['pk'])
        return HttpResponse(device.config)


class DeviceReadingsAPI(View):
    def post(self, request):
        data = json.loads(request.body)
        device = Device.objects.get(pk=data['pk'])
        readings = json.loads(device.readings)

        for key, value in data['readings'].items():
            readings[key] = value

        device.readings = json.dumps(readings)
        device.save()
        return HttpResponse('')


class IndexView(TemplateView):
    template_name = "macarons_app/index.html"


class AddScriptView(View):
    def get(self, request, *args, **kwargs):
        return render(request, "macarons_app/add_script.html")

    def post(self, request, *args, **kwargs):
        script = Script(name=request.POST.get('name'),
                        description=request.POST.get('description'))

        context = {'name': request.POST.get('name'),
                   'description': request.POST.get('description')}
        form_valid = True

        # main file
        try:
            main_file = request.FILES['main_file']
            if main_file.name.split('.')[-1] != 'py':
                form_valid = False
                context['main_error'] = 'Main file must be a .py file.'
            else:
                script.main = json.dumps([line.decode('utf-8').replace('\r', '') for line in main_file])
        except KeyError:
            form_valid = False
            context['main_error'] = 'Script must contain a main.py file.'

        # config file
        try:
            config_file = request.FILES['config_file']
            if config_file.name.split('.')[-1] != 'json':
                form_valid = False
                context['config_error'] = 'Config file must be a .json file.'
            else:
                config = json.loads(config_file.read().decode('utf-8'))
                config_type = {}
                for key in config:
                    config_type[key] = type(config[key]).__name__
                script.config_type = json.dumps(config_type)
                script.config = json.dumps(config)
        except KeyError:
            form_valid = False
            context['config_error'] = 'Script must contain a config.json file.'

        # readings file
        try:
            readings_file = request.FILES['readings_file']
            if readings_file.name.split('.')[-1] != 'json':
                form_valid = False
                context['readings_error'] = 'Readings file must be a .json file.'
            else:
                readings = json.loads(readings_file.read().decode('utf-8'))

                # todo: generate readingslog stuff, or do firebase thing
                script.readings = json.dumps(readings)
        except KeyError:
            form_valid = False
            context['readings_error'] = 'Script must contain a reading.json file.'

        if form_valid:
            script.save()   # need to save to generate device.pk (used in making directory)

            # create main.py file in script directory
            dir_path = 'macarons_app/management/commands/'
            with open(dir_path + f'main{script.pk}.py', 'w+') as f:
                for line in json.loads(script.main):
                    f.write(line)

            return redirect('update_script_view', pk=script.pk)
        else:
            return render(request, "macarons_app/add_script.html", context)


class ScriptListView(ListView):
    model = Script
    template_name = "macarons_app/script_list.html"


class ScriptUpdateView(View):
    def get(self, request, *args, **kwargs):
        script_pk = kwargs['pk']

        if 'run_script' in request.GET:
            execute_from_command_line((['manage.py', f'main{script_pk}', f'{script_pk}']))

        script = Script.objects.get(pk=script_pk)
        context = {'pk': script_pk,
                   'name': script.name,
                   'description': script.description,
                   'main': json.loads(script.main)}

        config = json.loads(script.config)
        config_type = json.loads(script.config_type)
        kvt = []
        for key in config:
            kvt.append([key, config[key], config_type[key]])
        context['kvt'] = kvt

        readings = json.loads(script.readings)
        context['readings'] = readings
        context['script_ready'] = readings['script_ready'][0]
        return render(request, "macarons_app/update_script.html", context)

    def post(self, request, *args, **kwargs):
        script = Script.objects.get(pk=kwargs['pk'])
        context = {'pk': kwargs['pk']}

        # general
        script.name = request.POST.get('name')
        context['name'] = script.name
        script.description = request.POST.get('description')
        context['description'] = script.description

        # config
        no_config_upload = True
        try:
            config_file = request.FILES['config_file']
            if config_file.name.split('.')[-1] != 'json':
                context['config_upload_error'] = 'Config file must be a .json file.'
            else:
                no_config_upload = False
                config = json.loads(config_file.read().decode('utf-8'))
                config_type = {}
                for key in config:
                    config_type[key] = type(config[key]).__name__
                script.config_type = json.dumps(config_type)
                script.config = json.dumps(config)

                kvt = []
                for key in config:
                    kvt.append([key, config[key], config_type[key]])
                context['kvt'] = kvt
        except KeyError:
            pass

        if no_config_upload:
            config = json.loads(script.config)
            config_type = json.loads(script.config_type)

            kvt = []
            incorrect_keys = []
            for i, key in enumerate(config):
                # boolean (value is constrained by drop down box to be either 't' or 'f', always correct type)
                if config_type[key] == 'bool':
                    value = request.POST.get(key) == 't'
                    kvt.append([key, value, config_type[key]])
                    if config[key] != value:
                        config[key] = value

                elif config_type[key] == 'int':
                    try:
                        value = int(request.POST.get(key))
                        kvt.append([key, value, config_type[key]])
                        if config[key] != value:
                            config[key] = value
                    except ValueError:
                        incorrect_keys.append(key)
                        kvt.append([key, config[key], config_type[key]])

                elif config_type[key] == 'float':
                    try:
                        value = float(request.POST.get(key))
                        kvt.append([key, value, config_type[key]])
                        if config[key] != value:
                            config[key] = value
                    except ValueError:
                        incorrect_keys.append(key)
                        kvt.append([key, config[key], config_type[key]])

                elif config_type[key] == 'str':
                    kvt.append([key, request.POST.get(key), config_type[key]])
                    if config[key] != request.POST.get(key):
                        config[key] = request.POST.get(key)

            script.config = json.dumps(config)
            context['kvt'] = kvt
            if incorrect_keys:
                context['config_error'] = 'Wrong variable type for: {}'.format(incorrect_keys)

        # readings
        no_readings_upload = True
        try:
            readings_file = request.FILES['readings_file']
            if readings_file.name.split('.')[-1] != 'json':
                context['readings_upload_error'] = 'Readings file must be a .json file.'
            else:
                no_readings_upload = False
                readings = json.loads(readings_file.read().decode('utf-8'))

                # todo: generate readingslog stuff, or do firebase thing
                script.readings = json.dumps(readings)
                context['readings'] = readings
                context['script_ready'] = readings['script_ready'][0]
        except KeyError:
            pass

        if no_readings_upload:
            readings = json.loads(script.readings)
            context['readings'] = readings
            context['script_ready'] = readings['script_ready'][0]

        # main
        try:
            main_file = request.FILES['main_file']
            if main_file.name.split('.')[-1] != 'py':
                context['main_error'] = 'Main file must be a .py file.'
            else:
                script.main = json.dumps([line.decode('utf-8').replace('\r', '') for line in main_file])
                context['main'] = script.main

                # create main.py file in script directory
                dir_path = 'macarons_app/management/commands/'
                with open(dir_path + f'main{script.pk}.py', 'w+') as f:
                    for line in json.loads(script.main):
                        f.write(line)
        except KeyError:
            pass

        context['main'] = script.main
        script.save()
        return render(request, "macarons_app/update_script.html", context)


class ScriptAPI(View):
    def post(self, request):
        data = json.loads(request.body)

        # reset script_pk of device to -1
        device_pk = data['device_pk']
        device = Device.objects.get(pk=device_pk)

        config = json.loads(device.config)
        config['script_pk'] = -1
        device.config = json.dumps(config)
        device.save()

        # tell script that this device is now done
        script_pk = data['script_pk']
        script = Script.objects.get(pk=script_pk)
        readings = json.loads(script.readings)
        readings['devices_done'][0][f'pk{device_pk}'] = True
        script.readings = json.dumps(readings)
        script.save()

        execute_from_command_line((['manage.py', f'main{script_pk}', f'{script_pk}']))
        return HttpResponse('')
