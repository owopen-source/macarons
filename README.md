
# MACARONS
<div align="center">

[![](./documentation/images/macarons_thumbnail.png)](https://drive.google.com/file/d/1bquN9AJVtha5Jdnhvae37s9XJSJ750oD/preview "MACARONS demo video.")

</div>

The modular automated crop array online system (MACARONS) is
an extensible, scalable, open hardware system for plant transport in automated horticulture systems such as vertical 
farms. The current system is designed to move trays of plants up to 1060mm x 630mm and 12.5kg at a rate of 10cm/s along the
guide rails and 4.17cm/s up the lifts, such as between stations for monitoring and actuating plants. The system is 
designed to be tessellated horizontally and vertically. A minimum system consisting of one horizontal unit and two 
vertical units was used as an example build in this work. The minimum system can be assembled by a technical person in 
approximately one day and costs approximately 1535.50 USD (in 2022).

The project includes:
- Hardware schematics
- Assembly instructions
- CAD models for the required 3D-printing and laser-cutting
- Software for the Django webserver backend
- Software for ESP32 (micropython) micro-controllers

To build the example minimal system, consult the [Bill of Materials](#bill-of-materials) and acquire the relevant parts.
Follow the instructions to for [Software Setup](#software-setup) and [Hardware Setup](#hardware-setup). Once the 
infrastructure has been setup, consult the [User Guide](#user-guide) for the operation of the framework and follow the 
instructions for setting up the MACARON system. Once the system is set up, follow the instructions in the 
[General Testing](#general-testing) section.

<div align="center">
	<img src="./documentation/schematics/macarons.png" alt="MACARONS" width="350"/>
</div> 

# Table of Contents
[Bill of Materials](#bill-of-materials)\
[Software Setup](#software-setup)\
[Hardware Setup](#hardware-setup)\
[User Guide](#user-guide)\
[General Testing](#general-testing)\
[How to Contribute?](#how-to-contribute)\
[Cite MACARONS](#cite-macarons)\
[Licence](#licence)


# Bill of Materials
The quantity and cost of all parts is given in the Table below.

| Part Name        | Qty           | Cost / USD | Source| Description|
| :--- |---:| ---:|:---|:---|
| 20x20x250|	9	|18.47|	[Ooznest](https://ooznest.co.uk/product/v-slot-linear-rail-20x20mm-cut-to-size/)|20mm x 20mm x 250mm aluminium extrusion| 
| 20x20x500	|10|	41.04|	[Ooznest](https://ooznest.co.uk/product/v-slot-linear-rail-20x20mm-cut-to-size/)|20mm x 20mm x 500mm aluminium extrusion|
| 20x20x750|	10|	61.56	|[Ooznest](https://ooznest.co.uk/product/v-slot-linear-rail-20x20mm-cut-to-size/)|20mm x 20mm x 750mm aluminium extrusion|
| 20x40x250|	2	|8.62|	[Ooznest](https://ooznest.co.uk/product/v-slot-linear-rail-20x40mm-cut-to-size/)|20mm x 40mm x 250mm aluminium extrusion|
| 2GT Pulley|	2	|7.40	|[Amazon](https://www.amazon.co.uk/gp/product/B084KXXS4W/)|20-teeth 2GT 6mm width 6mm bore belt pulley|
| 2GT Timing Belt Closed|	2	|8.00	|[Amazon](https://www.amazon.co.uk/gp/product/B07V6N32B1/)| 2GT 6mm width 200mm length closed timing belt|
| 40x40x1500|	10	|282.15|	[Ooznest](https://ooznest.co.uk/product/v-slot-linear-rail-40x40mm-cut-to-size/)| 40mm x 40mm x 1500mm aluminium extrusion|
| 40x40x250	|2|	9.42|	[Ooznest](https://ooznest.co.uk/product/v-slot-linear-rail-40x40mm-cut-to-size/)| 40mm x 40mm x 250mm aluminium extrusion|
| 40x40x40	|8|	6.02|	[Ooznest](https://ooznest.co.uk/product/v-slot-linear-rail-40x40mm-cut-to-size/)| 40mm x 40mm x 40mm aluminium extrusion|
| 40x40x500	|16	|150.50|	[Ooznest](https://ooznest.co.uk/product/v-slot-linear-rail-40x40mm-cut-to-size/)| 40mm x 40mm x 500mm aluminium extrusion|
| 40x40x750	|6	|84.70|	[Ooznest](https://ooznest.co.uk/product/v-slot-linear-rail-40x40mm-cut-to-size/)| 40mm x 40mm x 750mm aluminium extrusion|
| Arm	|2	|	- | Laser-Cut| - |
| Arm Spacer	|2	|	-| 3D-Printed| - | 
| Arm Wheel	|2	|	-| 3D-Printed| - |
| Ball Bearing	|38|	43.32	|[Ooznest](https://ooznest.co.uk/product/625-2rs-ball-bearing/)| 625-2RS ball bearing|
| Bearing house	|2	|-|	3D-Printed| -|
| Bearing spacer|	2	|	-| 3D-Printed|-| 
| Cable Clamps	|1|	8.00	|[Amazon](https://www.amazon.co.uk/YEYIT-Stainless-Tensioning-Household-Transportation/dp/B08F39237G/)|4mm cable clamps|
| Cast Angle Bracket	|10		| 31.35| [Ooznest](https://ooznest.co.uk/product/90-degree-angle-corner/)| Aluminium extrusion angle bracket|
| Cast Corner Bracket|	24|	41.04|	[Ooznest](https://ooznest.co.uk/product/90-degree-cast-corner/)| Aluminium extrusion corner bracket|
| Double Tee Nut|	28|	32.24	|[Ooznest](https://ooznest.co.uk/product/double-tee-nuts/)| Aluminium extrusion double tee nuts|
| Electric Winch|	1	|72.34|	[Amazon](https://www.amazon.co.uk/gp/product/B01AO2VVTM/)| 12V DC 0.6kWh / 0.8hp|
| Elevator Sensor Mount	|1	|	-| Laser-Cut| - |
| Enclosure | 1 |37.69 |Amazon| 380mm x 300mm x 120mm enclosure|
| ESP32|	2|	8.92|	Amazon| ESP32-C3-12F|
| Fuse (20A) | 1 | - | Comes with Fuse Box | - |
| Fuse Box | 1 | 15.66 |[Amazon](https://www.amazon.co.uk/gp/product/B07GYTZF35/)| - |
| Hammock Fixing|	1	|13.67|	[Amazon](https://www.amazon.co.uk/gp/product/B07FP3R1RF/)| M5 mounted hammock fixing|
| Heat Shrink Sleeve|	6|	|	Comes with Wires| - |
| Infrared Sensor|	2|	4.99	|[Amazon](https://www.amazon.co.uk/gp/product/B07TZ782F4/)| Infrared line following sensor|
| Interact Arm	|1	|	-| 3D-Printed|  - |
| Interact Case|	1|- | 3D-Printed	|  - |
| Joining Plate (Double)	|8|	18.24	|[Ooznest](https://ooznest.co.uk/product/2-hole-joining-plate/)| Two hole joining plate|
| Joining Plate (Triple)	|2|	5.72	|[Ooznest](https://ooznest.co.uk/product/3-hole-joining-plate/)| Three hole joining plate|
| Jumper wires (female female)|	3	|4.99|	[Amazon](https://www.amazon.co.uk/Jumper-Multicolored-Dupont-Breadboard-arduino/dp/B08X4QMWDF/)| -|
| L298N|	1	|2.40	|[Amazon](https://www.amazon.co.uk/Bridge-Stepper-Driver-Controller-Arduino/dp/B07T48R9VY/)| L298N motor driver |
| L-Bracket Double	|39|	60.47	| [Ooznest](https://ooznest.co.uk/product/universal-l-brackets/)| One hole L-bracket |
| L-Bracket Single|	22|	25.08| [Ooznest](https://ooznest.co.uk/product/universal-l-brackets/)	| Two hole L-bracket |
| Lock Arm | 1 | - | Laser-Cut| - |
| Lock Mount | 1 | - | Laser-Cut| - |
| L-Wheel Mount|	2	|- | Laser-Cut	| - | 
| M2.5 Nylon Standoff Set	|4	|10.48	|[Amazon](https://www.amazon.co.uk/gp/product/B0839HM1WX/)| - |
| M3 25mm Bolt	|9|	1.34	|[Ooznest](https://ooznest.co.uk/product/m3-socket-head-bolts/)| - |
| M3 8mm Bolt	|12	|1.78|	[Ooznest](https://ooznest.co.uk/product/m3-socket-head-bolts/)| - |
| M3 Drop-In Tee Nut	|3|	0.89|	[Ooznest](https://ooznest.co.uk/product/drop-in-tee-nuts/)| - | 
| M3 Hex Nut		|12| - |	Comes with Standoff Set|  - |
| M3 Nylon Standoff Set|	6|	10.25|	[Amazon](https://www.amazon.co.uk/gp/product/B091KJNZLB/)| -|
| M5 15mm Bolt|	10|	2.74	|[Ooznest](https://ooznest.co.uk/product/low-profile-m5-bolts/)|  -|
| M5 25mm Bolt	|16|	4.38	|[Ooznest](https://ooznest.co.uk/product/low-profile-m5-bolts/)|  -|
| M5 8mm Bolt	|272|	49.76|	[Ooznest](https://ooznest.co.uk/product/low-profile-m5-bolts/)|  -|
| M5 Drop-In Tee Nut|	286|	65.40|	[Ooznest](https://ooznest.co.uk/product/drop-in-tee-nuts/)|  -|
| M5 Grub Screws|	56		| - | Comes with Double Tee Nut|  -|
| M5 Hex Nut	|20|	1.37	|[Ooznest](https://ooznest.co.uk/product/hex-nuts/)|  -|
| Motor	|2	|31.98	|[Amazon](https://www.amazon.co.uk/gp/product/B071XG53B4/)| 12V DC 120 RPM 6mm shaft gear box motor |
| Motor Driver |1 | 22.81|[Amazon](https://www.amazon.co.uk/gp/product/B07H2MDXMN/)| 12V 30A motor driver|
| Mover Base|	1	| - | Laser-Cut	| - |
| Mover Base Bracket|	4	|-| 3D-Printed	| - | 
| Nylon Spacer 0.125 inch	|8|	2.37| [Ooznest](https://ooznest.co.uk/product/nylon-spacers/)| -|
| Nylon Spacer 0.25 inch|	12|	3.42| [Ooznest](https://ooznest.co.uk/product/nylon-spacers/)	| -| 
| Pillow Block	|4	|9.11	|[Amazon](https://www.amazon.co.uk/gp/product/B07QJYDVT7/)| 8mm bore pillow block |
| Power Connector|	1|	6.71	|[Amazon](https://www.amazon.co.uk/VCE-Connector-Adapters-Compatible-Security/dp/B077STNHRJ/)| - |
| Power Supply | 1 | 22.86| [Amazon](https://www.amazon.co.uk/gp/product/B072J97N8T/)| 12V 30A DC regulated power supply|
| Precision Shim	|54	|15.39	| [Ooznest](https://ooznest.co.uk/product/precision-shim/)| -|
| Raspberry Pi	|1|	40.47	|[The Pi Hut](https://thepihut.com/products/raspberry-pi-4-model-b)| Model 4B | 
| SG90 Servo|	2	|9.11	|[Amazon](https://www.amazon.co.uk/gp/product/B07MY2Y253/)| - |
| Shaft|	2|	9.11	|[Amazon](https://www.amazon.co.uk/gp/product/B07KZ833LY/)| 8mm shaft |
| Shaft Lock Collar|	4|	5.03	|[Amazon](https://www.amazon.co.uk/gp/product/B08BLLHLBZ/)| 8mm shaft lock collar| 
| Single Groove Pulley	|2	|31.19	|[Amazon](https://www.amazon.co.uk/gp/product/B07CC99N7N/)| 8mm single groove pulley|
| Tension Spring	|1|	10.12|	[Amazon](https://www.amazon.co.uk/gp/product/B0928CHJD7/)| 8.5mm x 47mm |
| V-Slot Fillers|	4	|13.68|	[Ooznest](https://ooznest.co.uk/product/slot-cover/)| - |
| V-Wheel|	16|	41.95|	[Ooznest](https://ooznest.co.uk/product/delrin-dual-v-wheel/)| V-wheel for aluminium extrusion |
| Wire Connector	|2	|11.39|	[Amazon](https://www.amazon.co.uk/gp/product/B091FLD6LV/)|  - |
| Wires	|4	|18.11|	[Amazon](https://www.amazon.co.uk/gp/product/B099JK3RH3/)| 18 AWG wires |






# Software Setup

The software versions used include Raspbian (buster), Python 3.8, Micro-Python 1.18 and Django 4.1.



### Raspberry Pi Django Setup
On the Raspberry Pi terminal run\
```sudo apt-get update && sudo apt-get upgrade```

Install Apache2 by running\
```sudo apt install apache2 -y```

Install the WSGI package by running\
```sudo apt install libapache2-mod-wsgi-py3```

Configure Apache for Django by edit the conf file
```sudo nano /etc/apache2/sites-enabled/000-default.conf```

Above the snippet\
```</VirtualHost>```

Paste
```Alias /static /home/pi/pidjango/static
    <Directory /home/pi/pidjango/static>
        Require all granted
    </Directory>

    <Directory /home/pi/pidjango/pidjango>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    WSGIDaemonProcess django python-path=/home/pi/pidjango python-home=/home/pi/pidjango/djenv
    WSGIProcessGroup django
    WSGIScriptAlias / /home/pi/pidjango/pidjango/wsgi.py
```

Restart Apache by running\
```sudo systemctl restart apache2```

This section was adapted from the following source, for further details see: https://pimylifeup.com/raspberry-pi-django/

### Backend Setup
Get the host IP address by running\
```hostname -I```

Open the file ```software/pidjango/pidjango/django_secrets.py``` and set ```host_ip``` to this value.\
Set ```ssid``` to the wifi username.\
Set ```password``` to the wifi password.

### Screen Setup
Install Screen by running\
```sudo apt install screen```

# Hardware Setup

### Custom Parts
The following items need to be 3D-printed or laser-cut. The links to the appropriate STEP or DXF files are given below. 
3mm perspex sheets were used for the laser-cut parts.

|Part Name| Qty | Process|
|---|---|---|
|Arm	|2| [Laser-Cut](/documentation/dxf_files/arm.dxf)|
|Arm Spacer	|2|[3D-Printed](/documentation/stl_files/arm_spacer.stl)|
|Arm Wheel	|2|[3D-Printed](/documentation/stl_files/arm_wheel.stl)|
|Bearing house	|2|[3D-Printed](/documentation/stl_files/bearing_house.stl)|
|Bearing spacer	|2|[3D-Printed](/documentation/stl_files/bearing_spacer.stl)|
|Elevator Sensor Mount	|1| [Laser-Cut](/documentation/dxf_files/elevator_sensor_mount.dxf)|
|Interact Arm|	1|[3D-Printed](/documentation/stl_files/interact_arm.stl)|
|Interact Case|	1|[3D-Printed](/documentation/stl_files/interact_case.stl)|
|Lock Arm| 1 |[Laser-Cut](/documentation/dxf_files/lock_arm.dxf)|
|Lock Mount| 1 |[Laser-Cut](/documentation/dxf_files/lock_mount.dxf)|
|L-Wheel Mount|	2|[3D-Printed](/documentation/stl_files/L_wheel_mount.stl)|
|Mover Base	|1| [Laser-Cut](/documentation/dxf_files/mover_base.dxf)|
|Mover Base Bracket|	4|[3D-Printed](/documentation/stl_files/mover_base_bracket.stl)|





### Small Assemblies
[Assembly details](/documentation/schematics/elevator_shelf_small.pdf) for assembling the smaller components for the shelf and elevator.

[Assembly details](/documentation/schematics/mover_small.pdf) for assembling the smaller components for the mover.


### Carriage
|Part Name| Qty |
|---|---|
|20x20x250|2|
|20x20x500|2|
|Ball Bearing|4|
|Cast Angle Bracket|4|
|L-Bracket Single|4|
|M5 25mm Bolt|4|
|M5 8mm Bolt|12|
|M5 Drop-In Tee Nut|12|
|M5 HexNut|4|
|Nylon Spacer 0.125 inch|4|
|Precision Shim|4|
|V-Wheel|4|


Construct the carriage as shown [here](/documentation/schematics/carriage.pdf).


### Shelf and Elevator Frames
|Part Name| Qty |
|---|---|
|20x20x250 |	5|
|20x20x500	|8|
|20x20x750	|10|
|40x40x1500	|10|
|40x40x250	|2|
|40x40x40	|8|
|40x40x500	|16|
|40x40x750	|6|
|Ball Bearing	|16|
|Cast Corner Bracket|20|
|Double Tee Nut	|28|
|Joining Plate	|4|
|L-Bracket Double	|39|
|L-Bracket Single	|18|
|M5 25mm Bolt	|8|
|M5 8mm Bolt	|240|
|M5 Drop-In Tee Nut|248|
|M5 Grub Screws	|56|
|Nylon Spacer 0.25 inch|8|
|Pillow Block	|4|
|Precision Shim	|24|
|Shaft	|2|
|Shaft Lock Collar|	4|
|Single Groove Pulley|	2|
|V-Wheel	|8|


- Construct Frame Assembly 1 as shown [here](/documentation/schematics/assembly1.pdf).
- Test that the carriage rolls smoothly along the outer rails. This guarantees that the rail spacing is correct. 
- Construct Frame Assembly 2 as shown [here](/documentation/schematics/assembly2.pdf).
- Test that the carriage rolls smoothly along the outer rails of the second floor.
- Construct Frame Assembly 3 as shown [here](/documentation/schematics/assembly3.pdf).
- Construct Frame Assembly 4 as shown [here](/documentation/schematics/assembly4.pdf).
- Construct Frame Assembly 5 as shown [here](/documentation/schematics/assembly5.pdf).
- Construct Frame Assembly 6 by aligning Frame Assembly 5 with Frame Assembly 4 as shown 
  [here](/documentation/schematics/assembly6.pdf).
- Construct Frame Assembly 7 as shown [here](/documentation/schematics/assembly7.pdf). 
- Check that the carriage rolls smoothly in Frame Assembly 5. 
- Check that the carriage rolls smoothly from Frame Assembly 5 onto Frame Assembly 4, this ensures that the rails are 
  aligned properly.
- Check that there is 3mm between the edge of Frame Assembly 4 and Frame Assembly 5 by sliding a 3mm plastic sheet 
  between the two assemblies. Adjust the V-Wheel Assemblies accordingly.    

### Shelf and Elevator Peripherals
|Part Name| Qty |
|---|---|
|Cable Clamps|1|
|Electric Winch|	1|
|Elevator Sensor Mount|	1|
|Enclosure | 1 |
|ESP32 | 1 |
|Fuse (20A) | 1 |
|Fuse Box | 1 |
|Hammock Fixing|	1|
|Heat Shrink Sleeve|	6|
|Infrared Sensor	|1|
|Joining Plate (Double)	|2|
|Joining Plate (Triple)	|2|
|Jumper wires (female female)|	3|
|Lock Arm | 1 |
|Lock Mount | 1 |
|M3 25mm Bolt	|2|
|M3 Hex Nut	|2|
|M3 Nylon Standoff Set|	1|
|M5 15mm Bolt	|2|
|M5 8mm Bolt	|10|
|M5 Drop-In Tee Nut|	12|
|Precision Shim|3|
|Raspberry Pi	|1|
|V-Slot Fillers	|4|
|Wires (2200mm)	|4|

- Cut four wires of length 2200mm and solder female jumper wires on to both ends. 
- Run the wires through the drag chain.    
- Attach the drag chain to the location specified: [view1](/documentation/schematics/drag_chain.pdf), 
  [view2](/documentation/images/pic_drag_chain.png) and [view3](/documentation/images/pic_drag_chain.png).
- Test the mobility of the drag chain by moving the elevator platform up and down.
- Add V-slot fillers to the [locations](/documentation/schematics/fillers.pdf) and add gaps as shown 
  [here](/documentation/images/pic_filler_gap.png).
- Attach the infrared sensor to the elevator sensor mount and attach the wires as shown 
  [here](/documentation/images/pic_elevator_sensor.png).    
- Run the wires through additional fillers as shown [here](/documentation/images/pic_sensor_to_drag.png).
- Attach the electric winch to the elevator and the cable to the elevator platform as shown 
  [here](/documentation/images/pic_winch.png).    
- Make sure that the cables are vertical by adjusting the pulleys.
- Make sure that all tee nuts are locked-in securely.
- Add lock assemblies to the [elevator platform](/documentation/images/lock_location.pdf) as shown 
  [here](/documentation/images/lock.png). 


### Elevator Electronics

- Connect the motor driver to the other components as shown below.
  
  |Motor Driver        | Other Components|
  | --- |---|
  |ENB| ESP32 GPIO19|
  |IN1B| ESP32 GPIO18|
  |IN2B| ESP32 GPIO10|
  |GND| ESP32 GND|
  |B+|Electric Winch +|
  |B-| ElectricWinch -|
  |PWR+|Fuse Box Out +|
  |PWR-|Fuse Box Out -|

- Connect the fuse box to the other components as shown below and add a fuse (20A) to the fuse box.
  
  |Fuse Box        | Other Components|
  | --- |---|
  |Fuse Box In +| Power Supply +|
  |Fuse Box In -| Power Supply -|
  
- Connect the infrared sensor to the other components as shown below. The D0 pin is connected using a wire in the drag 
chain. Use a three wire connector for the 5V connector and the GND connector.
  
  |Infrared Sensor        | Other Components|
  | --- |---|
  |VCC| 5V Connector|
  |D0| ESP32 GPIO0|
  |GND| GND Connector|

- Connect the SG90 servo to the other components as shown below. The PWM pin is connected using a wire in the drag chain.
  
  |SG90        | Other Components|
  | --- |---|
  |PWM (orange)| ESP32 GPIO9|
  |VCC (red)| 5V Connector|
  |GND (black)| GND Connector|


- Connect the ESP32 to the other components as shown below. Use the remaining wires in the drag chain.
  
  |ESP32        | Other Components|
  | --- |---|
  |5V| 5V Connector|
  |GND| GND Connector|

- Tuck the wire connectors into the aluminium extrusion near the infrared sensor as shown 
  [here](/documentation/images/tucked_wires.png).


- Power the ESP32 using the micro-usb power supply and adjust the elevator infrared sensor (up and down) such that it is 
  on the verge of triggering when the elevator is aligned with each floor.
- Power the ESP32 using the Micro-USB power supply.
- There are two green lights on the sensor one indicating that power is on and the other indicating that a reflection is 
  detected. Check that the sensor is operational. The detection light should turn on when the sensor is obstructed by an aluminium profile. The sensitivity of the sensor can be adjusted by tightening/loosening the screw on the sensor.
- Align the sensor by adjusting its location on the elevator sensor mount such that it is on the verge of triggering 
  when the elevator platform is aligned with the shelf.
  
- Mount the power supply, motor driver, fuse box and ESP32 in to the enclosure and wire them up as shown [here](/documentation/images/control_box.png).
  <br>**DO NOT PLUG IN THE MAINS CABLE UNTIL THE ENCLOSURE IS CLOSED. MAKE SURE THAT THE POWER SUPPLY COVER IS SHUT.**
  
- Close the enclosure lid as shown in [here](/documentation/images/closed_box.png).



### Mover
|Part Name| Qty |
|---|---|
|20x20x250	|2|
|20x40x250	|1|
|2GT Pulley	|2|
|Arm	|2|
|Arm Spacer|	2|
|Arm Wheel |	2|
|Ball Bearing|	14|
|Bearing house|	2|
|Bearing spacer|	2|
|Cast Angle Bracket|	6|
|Cast Corner Bracket|	4|
|Interact Arm	|1|
|Interact Case|1|
|Joining Plate	|4|
|L-Wheel Mount	|2|
|M3 25mm Bolt	|7|
|M3 8mm Bolt	|12|
|M3 Drop-In Tee Nut|	2|
|M3 Hext Nut|11|
|M5 15mm Bolt|	8|
|M5 25mm Bolt|	4|
|M5 8mm Bolt	|18|
|M5 Drop-In Tee Nut|	16|
|M5 Hex Nut	|16|
|Motor	|2|
|Mover Base|	1|
|Mover Base Bracket	|4|
|Nylon Spacer 0.125 inch|	4|
|Nylon Spacer 0.25 inch	|4|
|Precision Shim	|18|
|SG90 Servo	|1|
|V-Wheel	|4|

- Construct Left Arm Assembly 1 as shown [here](/documentation/schematics/arm1.pdf).
- Construct Left Arm Assembly 2 as shown [here](/documentation/schematics/arm2.pdf).
- Construct Left Arm Assembly 3 as shown [here](/documentation/schematics/arm3.pdf).
- Construct Right Arm Assembly as shown [here](/documentation/schematics/arm4.pdf).
- Construct Mover Assembly 1 as shown [here](/documentation/schematics/mover1.pdf).
- Construct Mover Assembly 2 as shown [here](/documentation/schematics/mover2.pdf).
- Construct Mover Assembly 3 as shown [here](/documentation/schematics/mover3.pdf).
- Construct Mover Assembly 4 by attaching the spring to the specified locations and adjusting the M3 8mm bolts to control 
  the compactness of the mover as shown [here](/documentation/schematics/mover4.pdf).
- Construct Mover Assembly 5 as shown [here](/documentation/schematics/mover5.pdf).

### Mover Electronics
- Connect the L298N motor driver to the other components as below. Use a five wire connector for the GND connector.
  
  |L298N   | Other Components|
  | --- |---|
  |IN1| ESP32 GPIO18|
  |IN2| ESP32 GPIO10|
  |ENA| ESP32 GPIO19|
  |IN3| ESP32 GPIO9|
  |IN4| ESP32 GPIO8|
  |ENB| ESP32 GPIO6|
  |OUT1| Left Motor +|
  |OUT2| Left Motor -|
  |OUT3| Right Motor -|
  |OUT4| Right Motor +|
  |12V| Power Connector +|
  |GND| GND Connector|

- Connect the infrared sensor to the other components as shown below. Use a five wire connector for the 5V connector.
  
  |Infrared Sensor  | Other Components|
  | --- |---|
  |D0| ESP32 GPIO7|
  |VCC| 5V Connector|
  |GND| GND Connector|

- Connect the servo to the other components as shown below.
  
  |Servo  | Other Components|
  | --- |---|
  |PWM (orange)| ESP32 GPIO0|
  |VCC (red)| 5V Connector|
  |GND (black)| GND Connector|
  
- Connect the ESP32 to the other components as shown below.
  
  |ESP32  | Other Components|
  | --- |---|
  |5V| 5V Connector|
  |GND| GND Connector|

### Mover Peripherals
|Part Name| Qty |
|---|---|
|2GT Pulley|2|
|2GT Timing Belt Closed|2|
|ESP32	|1|
|Infrared Sensor|1|
|L298N	|1|
|M2.5 Nylon Standoff Set|4|
|M3 Drop-In Tee Nut	|1|
|M3 Nylon Standoff Set|	5|
|Power Connector	|1|
|Precision Shim	|1|
|Tension Spring	|1|
|Wire Connector	|2|


- Add the pulley and belt to the mover arms as shown [here](/documentation/images/mover_peripheral.png).
- Attach the infrared sensor to the right side of the mover as shown [here](/documentation/images/mover_peripheral.png).
- Mount the motor driver and the ESP32 on standoffs and tidy up the wires as shown 
  [here](/documentation/images/mover.png).


# User Guide
## Server
### Run Server
On the Raspberry Pi terminal, start a screen session by running\
```screen```

Change directory to ```software/pidjango```.

Run the server by running \
```python manage.py runserver 0.0.0.0:8000```

Detach from the screen session by pressing `ctrl` + `a` + `d`.

The website can now be accessed through a web-browser on any device connected to the same wifi network by typing in the 
host IP address.

### Kill Server
List the current screen sessions by running \
```screen -ls```

Attach back onto the screen with the `<session_id>` where the server is hosted by running\
```screen -r <session_id>```

Kill the server by pressing `ctrl` + `c`.

Exit and kill the screen session by running\
```exit```

## Devices
### Add Device
- Connect the ESP32 to the Raspberry Pi server using a USB connector.
- On the website, click on the add device button on the navigation bar and fill in the details.
- Once submitted, the registration procedure will start. Do not refresh or click on anything until redirected to a new 
  page.
  
### Edit Configurations
Device configurations can be updated on the corresponding device's detail page. 

Edit the relevant values and press save.

### Rewrite
Each device can be completed rewritten on the corresponding device's detail page.

Upload the `main.py`, `config.json` and `readings.json` files and press save.

## Scripts
### Add Script
On the website, click on the add script button on the navigation bar and fill in the details.
  
### Edit Configurations
Script configurations can be updated on the corresponding scripts's detail page. 

Edit the relevant values and press save.

### Rewrite
Each script can be completed rewritten on the corresponding script's detail page.

Upload the `main.py`, `config.json` and `readings.json` files and press save.

## MACARONS Setup
### Add Elevator Device
- Connect the Elevator ESP32 to the Raspberry Pi server.
- Add the device using the files from the `software/devices/elevator` directory.
- Disconnect the Elevator ESP32 once complete.
- Note down the pk value for the elevator from the elevator's details page.

### Add Mover Device

- Connect the Elevator ESP32 to the Raspberry Pi server.
- Add the device using the files from the `software/devices/mover` directory.
- Disconnect the Mover ESP32 once complete.
- Note down the pk value for the mover from the mover's details page.
  
### Add 'Go to Target' Script
- Edit the `main.py` file in the `software/scripts/go_to_target` directory by setting corresponding value
  for `mover_pk` and `elevator_pk`.
- Add a script using the files from the `software/scripts/go_to_target` directory.
  
### Add 'Deliver Parcel' Script
- Edit the `main.py` file in the `software/scripts/deliver_parcel` directory by setting corresponding value
  for `mover_pk` and `elevator_pk`.
- Add a script using the files from the `software/scripts/deliver_parcel` directory.

## MACARONS Operation
### Elevator
The ```awake``` flag is used to set the elevator to awake mode, allowing for pseudo-real-time control and for scripts
to control the device.

The ```grace_time``` is the amount of time (seconds) the elevator ignores any readings once a movement starts. This
prevents the elevator from stopping immediately when detecting the current floor's shelf edge.

```t_delta``` is the amount of time used for moving the platform into the load and unload position.

```v_target``` is used with the ```go_to_target``` flag. The elevator will move to this target value (0 or 1 for bottom 
and top floor, respectively) when the ```go_to_target``` flag is set.

The```go_to_target``` flag is used to send the elevator to the ```v_target```.

### Mover
The ```awake``` flag is used to set the mover to awake mode, allowing for pseudo-real-time control and for scripts
to control the device.

`overshoot_time` is the amount of time (seconds) the mover overshoots the target by. This is to approximately align the parcel's
centre with the V-slot filler gap.

The `grace_time` is the amount of time (seconds) the mover ignores any readings once a movement starts. This
prevents the mover from stopping immediately when detecting the gap corresponding to the current position.

The `interact` flag is set to raise the interaction arm.

The `move` flag is set to start the mover's motion.

The `forward` flag is used to set the direction the mover will move in.

`h_target` is used with the `go_to_target` flag. The mover will move to this target value (0 or 1 for elevator 
and shelf, respectively) when the `go_to_target` flag is set.

The `deliver` flag is used with the `go_to_target` flag. The mover will set the interaction arm according to whether it
is set to deliver a parcel or not.

The `go_to_target` flag is used to send the mover to the `h_target`.

### Go to Target
The script coordinates the mover and the elevator to send the mover to the target given by 
`target_h_pos` and `target_v_pos`. Both being either 0 or 1. Before running the script make sure that both devices are awake.

### Deliver Parcel
The script coordinates the mover and the elevator to send mover the parcel to the top or ground floor.
The `target_ground_floor` flag indicates that the target is the ground floor, otherwise the target is the top floor.





# General Testing
Non-optional assembly integration tests are done throughout the hardware setup of the MACARON system which guarantees that 
the different component is assembled correctly. This section goes through the remaining non-optional testing of the system once the initial setup is completed.

### Mover Testing
- Place the mover on a flat surface.
- Power the mover using the power connector. The light on the Mover ESP32 and Motor Driver should turn on.
- Set and save the configuration `awake = True`.
- Set and save the configuration `move = True` and `forward = True`. The mover wheels should spin in opposite directions 
  such that when loaded on the rail it will push the mover forward.
- Set and save the configuration `move = True` and `forward = False`. The mover wheels should spin in opposite 
  directions such that when loaded on the rail it will push the mover backwards.
- Set and save the configuration `move = False`, `forward = True`. This will stop the motors.
- Set and save the configuration `interact = True`. The interaction arm should move to the vertical position.
- Set and save the configuration `interact = False`. The interaction arm should move to the horizontal position.
- Place the mover on the rails. 
- Align the infrared sensor with the v-slot filler. The infrared sensor trigger LED should be off.
- Align the infrared sensor with the gap in the v-slot filler. The infrared sensor trigger LED should be on. If this is 
  not the case, adjust the infrared sensor by tightening/loosening the screw on the sensor.
- Set `awake = False`. The mover test is complete.

### Elevator Testing
- Turn the elevator power on. The motor driver LED should light up.
- Power the elevator ESP32. The LED should light up.
- Unlock the electric winch by pulling out the pin as shown in Figure \ref{fig:winch_lock}. This will allow the motor to spin but will not pull on the cable.
- Set and save the configuration `awake = True` and `go_to_target = True`.
- Set and save the configuration `v_target = 1`. The motor should spin in the direction where it will move the elevator platform upwards. Wait for 5 seconds then obstruct the elevator sensor for 2 seconds and then stop. The electric winch should stop.
- Set and save the configuration `v_target = 0`. The motor should spin in the direction where it will move the elevator platform downwards. Wait for 5 seconds then obstruct the elevator sensor. The electric winch should stop immediately.
- Set and save the configuration `awake = False` and `go_to_target = False`. The elevator test is complete.

### Validation Testing
- Place the mover on elevator platform near the centre.
- For the mover, set `awake = True`.
- For the elevator, set `awake = True`.
- In the `go to target` script set and save the configuration `target_h_pos = 1` and `target_v_pos = 0`. Run the script.
  The mover should move onto the bottom shelf taking approximately 15s.
- In the `go to target` script set and save the configuration `target_h_pos = 1` and `target_v_pos = 1`. Run the script.
  The mover should move on to the top shelf with the assistance of the elevator. The vertical motion should take approximately 12s.
- In the `go to target` script set and save the configuration `target_h_pos = 0` and `target_v_pos = 1`. Run the script.
  The mover should move on to the bottom shelf with the assistance of the elevator.
- Place the carriage on the bottom shelf, above the mover.
- In the `deliver parcel` script set and the configuration `target_ground_floor = False`. Run the script. The mover 
  should interact with the carriage and place it on the top shelf with the assistance of the elevator.
- In the `deliver parcel` script set and the configuration `target_ground_floor = True`. Run the script. The mover 
  should interact with the carriage and place it on the bottom shelf with the assistance of the elevator.
- Add a 12.5kg payload onto the carriage and repeat the above commands.
- The validation test is complete.

# How to Contribute? 
Please get involved, we would really love to have your contribution! Extensions and improvements to the current system are very welcomed and will be merged into the project. Please fork our project and incorporate components and designs from MACARONS into your own applications, which may be completely different from agriculture. Please get in touch with us for an informal chat before doing the above so that we can help with your project: 

Here are some ideas for easy potential improvements for new contributors:
- Software optimisation for the control of MACARONS
- Incorporation of a lighting system
- Incorporation of an irrigation system

# Cite MACARONS
Please find the paper here:
```
https://openhardware.metajnl.com/articles/10.5334/joh.53/
```

Please also cite the work:
```
@article{wichitwechkarn2023macarons,
  title={MACARONS: A Modular and Open-Sourced Automation System for Vertical Farming},
  author={Wichitwechkarn, Vijja and Fox, Charles and others},
  journal={Jounral of Open Hardware},
  year={2023},
  publisher={Ubiquity Press}
}
```



# Licence
This work is provided under CERN-OHL-W licence for hardware design, GPL-3 licence for software source code and CC-BY4 licence for documentation including build instructions.

Disclaimer: Neither the authors nor the University of Lincoln and the University of Cambridge are repsonsible for accidents, injuries or
damage caused by this system, and by downloading, building or operating the design you agree to do so entirely 
at your own risk. The design is not a legal product and carries no safety certification.

